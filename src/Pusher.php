<?php

namespace elementfani\pusher;

use yii\base\Component;

/**
 * Created by PhpStorm.
 * User: elementfani
 * Date: 01/06/17
 * Time: 20:23
 */
class Pusher extends Component
{
    /**
     * @var int
     */
    public $appId;

    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $secret;

    /**
     * @var array
     */
    public $options = [];

    /**
     * @var \Pusher
     */
    private $_pusher;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        foreach (['appId', 'key', 'secret'] as $attribute)
        {
            if (!$this->$attribute)
            {
                throw new \Exception("Pusher {$attribute} is required");
            }
        }

        $this->_pusher = new \Pusher($this->key, $this->secret, $this->appId, $this->options);
    }

    /**
     * Trigger an event by providing event name and payload.
     * Optionally provide a socket ID to exclude a client (most likely the sender).
     *
     * @param array $channels An array of channel names to publish the event on.
     * @param string $event
     * @param mixed $data Event data
     * @param string $socketId [optional]
     * @param bool $debug [optional]
     * @param bool $alreadyEncoded
     * @return bool|string
     */
    public function push($channels, $event, $data = [], $socketId = null, $debug = false, $alreadyEncoded = false )
    {
        return $this->_pusher->trigger($channels, $event, $data, $socketId, $debug, $alreadyEncoded);
    }

    /**
     * Simple push for a specific user
     *
     * @param int $userId
     * @param string $event
     * @param mixed $data Event data
     * @param string $socketId [optional]
     * @param bool $debug [optional]
     * @param bool $alreadyEncoded
     * @return bool|string
     */
    public function pushUser($userId, $event, $data = [], $socketId = null, $debug = false, $alreadyEncoded = false )
    {
        $channel = $this->getUserChannelName($userId);
        return $this->_pusher->trigger($channel, $event, $data, $socketId, $debug, $alreadyEncoded);
    }

    /**
     * User channel based on his ID
     *
     * @param int $id
     * @return string
     */
    public function getUserChannelName($id)
    {
        return 'user_' . md5($this->secret . $id);
    }

    /**
     *Fetch channel information for a specific channel.
     *
     * @param string $channel The name of the channel
     * @param array $params Additional parameters for the query e.g. $params = array( 'info' => 'connection_count' )
     * @return object
     */
    public function getChannelInfo($channel, $params = array() )
    {
        return $this->_pusher->get_channel_info($channel, $params);
    }

    /**
     * Fetch a list containing all channels
     *
     * @param array $params Additional parameters for the query e.g. $params = array( 'info' => 'connection_count' )
     * @return array
     */
    public function getChannels($params = array())
    {
        return $this->_pusher->get_channels($params);
    }
}