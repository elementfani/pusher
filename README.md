Yii2 pusher
===================

INSTALL
-------
php composer.phar require elementfani/pusher

USAGE
-------

    1.  Add a new component into yor config file:
```
'components' => [
    'pusher' => [
        'class' => 'elementfani\yii2Pusher\Component',
        'appId' => '***', // get your credentials on pusher.com
        'key' => '***',
        'secret' => '***',
        'options' => [],
    ]
]
```

    2.  Now you can send a notification:
```
Yii::$app->pusher->push("main_channel", "hello all", ['someData' => 'someValue']);
```